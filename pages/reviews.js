import React from "react";
import { AppRegistry, Text, StyleSheet, View, Button } from "react-native";

export default class Reviews extends React.Component{

    static navigationOption = {
        title: null
    }

    render() {
        return (
            <View style={styles.container}>
                <Text>Review</Text>
            </View>
        );
    }

}

// Style
const styles = StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: "center",
      alignItems: "center",
      backgroundColor: "#F5FCFF"
    },
    welcome: {
      fontSize: 20,
      textAlign: "center",
      margin: 10
    }
  });