/**
|--------------------------------------------------
| Index Page for ios and Android
|--------------------------------------------------
*/

import React from "react";
import { AppRegistry, StyleSheet, View } from "react-native";
import { Container, Content, Text, Button, Card, Header, Footer, FooterTab, Icon } from "native-base";

// import HeaderHomepage from '../components/header-home';

// export class App
export default class App extends React.Component {
  // Set Navigation title
  static navigationOptions = {
    header: null,
    title: "Welcome"
  }

  // Render
  render() {
    const { navigate } = this.props.navigation;

    return (
      <Container>
        {/* Header */}
        {/* <HeaderHomepage/> */}
        {/* <Header style={styles.header}>
          <Text style={styles.headerTitle}>Jagatplay</Text>
        </Header> */}

        <Header androidStatusBarColor="orange" style={styles.header}>
            <Text style={styles.headerTitle}>Jagatplay</Text>
        </Header>

        <Content padder>
          <Text>Welcome to the Demo!</Text>
        </Content>

        <Footer tabActiveBgColor="orange" tabBarTextColor="orange" tabBarActiveTextColor="orange">
          <FooterTab>
            <Button>
              <Icon name="home" />
              <Text>Home</Text>
            </Button>
            <Button>
              <Icon name="home" />
              <Text>Home</Text>
            </Button>
            <Button>
              <Icon name="camera" />
              <Text>News</Text>
            </Button>
            <Button onPress={() => navigate("Reviews")}>
              <Icon active name="navigate" />
              <Text>Reviews</Text>
            </Button>
            <Button>
              <Icon name="person" />
              <Text>Lol</Text>
            </Button>
          </FooterTab>
        </Footer>
      </Container>
    );
  }
}

// Style
const styles = StyleSheet.create({
  content: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#F5FCFF"
  },
  welcome: {
    fontSize: 20,
    textAlign: "center",
    margin: 10
  },
  header: {
    backgroundColor: "orange",
    paddingTop: 30
  },
  headerTitle: {
    color: "white"
  },
  footer:{
    backgroundColor: "orange"
  }
});

// export default App;
