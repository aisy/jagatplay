import React, { Component } from 'react';
import { AppRegistry, Text, StyleSheet, View, Button } from "react-native";

export default class news extends Component {

    static navigationOption = {
        title : null
    }    

    render() {
        return (
            <Text>News</Text>
        );
    }
}

// Style
const styles = StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: "center",
      alignItems: "center",
      backgroundColor: "#F5FCFF"
    },
    welcome: {
      fontSize: 20,
      textAlign: "center",
      margin: 10
    }
  });


