import React from 'react';
import { AppRegistry } from 'react-native';
import { Header, Text } from 'native-base';

export class HeaderHome extends React.Component{

    render() {
        return (
            <Header>
                <Text>Jagatplay</Text>
            </Header>
        );
    }

}

AppRegistry.registerComponent('jagatplay', () => HeaderHome);