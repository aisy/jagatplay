/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import { AppRegistry } from 'react-native';
import { StackNavigator } from "react-navigation";

// import page
import App from './pages/index';
import Reviews from './pages/reviews';

// create navigation
export const SimpleApp = StackNavigator({
  Home:    { screen: App },
  Reviews: { screen: Reviews },
  // News:    { screen: News },
  // TopList: { screen: TopList }
}
);

AppRegistry.registerComponent('jagatplay', () => SimpleApp);
